<?php
$debug = $modx->getOption('debug', $scriptProperties, false);
  
if ($debug) {
    $modx->setLogLevel(modX::LOG_LEVEL_DEBUG);
}
//new user object
$user = $modx->newObject('modUser');
//get all form fields
$formFields = $hook->getValues();
//get user fields
$userFields = array_intersect_key($formFields, array('username' => '', 'password' => '', 'active' => ''));
//create md5 hash for password
$userFields[password] = md5($userFields[password]);
  
//get user profile fields
$userProfileFields = array_intersect_key($formFields, $modx->getFields('modUserProfile'));
  
//get user profile extended fields
$extendedFields = $modx->getOption('extendedFields', $scriptProperties, false);
$modx->log(modX::LOG_LEVEL_DEBUG, '[$extendedFields]: ' . $extendedFields);
if ($extendedFields) {
    $extendedFieldsArray = explode(',', $extendedFields);
    if (is_array($extendedFieldsArray)) {
        $userProfileExtendedFields = array_intersect_key($formFields, array_flip($extendedFieldsArray));
    }
}

if ($debug) {
    foreach ($hook->getValues() as $key => $value) {
        $modx->log(modX::LOG_LEVEL_DEBUG, '[$formFields] ' . $key . '=' . $value);
    }
    foreach ($userFields as $key => $value) {
        $modx->log(modX::LOG_LEVEL_DEBUG, '[$userFields] ' . $key . '=' . $value);
    }
    foreach ($userProfileFields as $key => $value) {
        $modx->log(modX::LOG_LEVEL_DEBUG, '[$userProfileFields] ' . $key . '=' . $value);
    }
    if (isset($userProfileExtendedFields) and is_array($userProfileExtendedFields)) {
        foreach ($userProfileExtendedFields as $key => $value) {
            $modx->log(modX::LOG_LEVEL_DEBUG, '[$userProfileExtendedFields] ' . $key . '=' . $value);
        }
    }
}



$user->fromArray($userFields); //username,password,active


if ($user->save() == false) {
    $modx->log(modX::LOG_LEVEL_ERROR, '[FormIt2AddUser] can\'t save user');
    return false;
}

// create user profile
$user->profile = $modx->newObject('modUserProfile');
$user->profile->fromArray($userProfileFields);
$user->profile->set('internalKey', $user->get('id'));

if (isset($userProfileExtendedFields) and is_array($userProfileExtendedFields)) {
    $user->profile->set('extended', $userProfileExtendedFields);
}

if ($user->profile->save() == false) {
    $user->remove();
    $modx->log(modX::LOG_LEVEL_ERROR, '[FormIt2AddUser] can\'t save user prtofile');
    return false;
}

return true;